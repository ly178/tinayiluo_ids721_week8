# tinayiluo_IDS721_week8

[![pipeline status](https://gitlab.com/ly178/tinayiluo_ids721_week8/badges/main/pipeline.svg)](https://gitlab.com/ly178/tinayiluo_ids721_week8/-/commits/main)

# Rust Command-Line Tool for Airline Safety Data Ingestion and Processing

This project develops a Rust command-line tool designed for the ingestion, transformation, loading (ETL), and querying of the AirlineSafety dataset. It demonstrates comprehensive data handling in Rust, from extracting data from a URL, transforming and loading it into an SQLite database, to performing various database queries.

## Requirements

- Rust programming language
- Cargo (Rust's package manager)
- SQLite
- Reqwest, Rusqlite, Csv, Sys-info, and other dependencies as listed in `Cargo.toml`

## Installation

First, ensure Rust and Cargo is installed. If not, install them by following the instructions on the [official Rust site](https://www.rust-lang.org/tools/install).

Clone the repository to the local machine:

```
git clone https://github.com/ly178/tinayiluo_ids721_week8.git
cd tinayiluo_ids721_week8
```

Then, build the project using Cargo:

```
cargo build --release
```

## Project Structure and Breakdown

### `lib.rs` - Core Functionality

- **Data Extraction (`extract` function):** Downloads the AirlineSafety dataset from a provided URL and saves it to a specified path, ensuring data availability for processing.
- **Data Transformation and Loading (`transform_load` function):** Reads the extracted CSV data, transforms it according to the schema of the AirlineSafety database, and loads it into an SQLite database. This step prepares the data for querying.
- **Data Querying (`query` function):** Executes SQL queries against the populated SQLite database, allowing for the retrieval and manipulation of airline safety records.

### `main.rs` - Command-Line Interface

Provides a CLI for interacting with the core functionality. It supports three actions: `extract`, `transform_load`, and `query`, each corresponding to the functionalities defined in `lib.rs`. It also includes logging of operations and performance metrics.

### `Cargo.toml` and `Makefile`

- **`Cargo.toml`**: Specifies project dependencies and metadata, ensuring reproducible builds and dependency management.
- **`Makefile`**: Defines shortcuts for common operations such as building, testing, formatting code, and running predefined database operations (create, read, update, delete).

### CI/CD Pipeline

- **Build and Test**: The code is built and tested within an Ubuntu environment, leveraging specific versions of Rust, Zig, and other tools.Test results and other artifacts are generated and captured.
- **Push Back to Remote**: If the build and test stage is successful, changes are pushed back to the GitLab repository by the CI pipeline, automating the integration of tested code.

### Testing

Unit tests are implemented for each core function (`extract`, `transform_load`, and `query`), ensuring the reliability and correctness of the tool's operations.

For detailed unit test results, see [Test Results](./test.md).

## Usage

Run the tool with the desired action (extract, transform_load, or query) followed by any necessary arguments. For example:

```
cargo run extract
cargo run transform_load
cargo run query "SELECT * FROM AirlineSafetyDB WHERE airline = 'Alaska Airlines';"
```

## Testing

Execute the unit tests to verify the correctness of the implementation:

```
cargo test
```

This project emphasizes practical Rust application for data processing tasks, demonstrating the language's power in building efficient, reliable command-line tools for ETL processes.

## Pipelines 

![Screen_Shot_2024-03-31_at_9.20.33_PM](/uploads/00e9220d57d5a37e367d019bad156127/Screen_Shot_2024-03-31_at_9.20.33_PM.png)



